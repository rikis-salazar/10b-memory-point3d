#include <iostream>

using std::cout;          

// Point3D needs three coordinates
const unsigned int SIZE = 3;


class Point3D{
   private:
      double* coords; 

   public:
       Point3D(); 
       ~Point3D(); 
       Point3D( const Point3D& ); 
       Point3D& operator=( const Point3D& );

       // Not needed here. Why?
       // double& operator[](int i) { return coords[i]; }

       // What about this one?
       const double& operator[](int i) const { return coords[i]; }
};



/**   T H E   B I G   4   ( D E F I N I T I O N S )   */
Point3D::Point3D(){ 
   cout << "Constructor (default)\n";
   coords = new double[SIZE];  
}

Point3D::~Point3D(){ 
   cout << "Destructor\n";
   delete[] coords;
}

Point3D::Point3D( const Point3D& source ){ 
   cout << "Copy Constructor\n";

   // Construction and copy
   coords = new double[SIZE];  
   for ( size_t i = 0 ; i < SIZE ; ++i ){
      this->coords[i] = source[i];     // <-- Now it works!!!
   }
}


Point3D& Point3D::operator=( const Point3D& rhs ){ 
   cout << "Assignment operator\n";

   // Why work ... ?
   if ( this != &rhs ) {
      // release memory from implicit object
      delete[] coords;

      // request memory for implicit object
      coords = new double[SIZE];

      // tranfer data
      for ( size_t i = 0 ; i < SIZE ; ++i ){
         this->coords[i] = rhs.coords[i];
      }

   }

   return *this; 
}



/**   T E S T E R   */
int main(){

   Point3D p0;
   Point3D clone1(p0); 
   Point3D clone2 = clone1 ; 
   clone2 = clone1;

   return 0;
}

