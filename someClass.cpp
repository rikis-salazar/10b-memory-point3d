/**
    Author: Ricardo Salazar
    Date: October 2016

    Description: The Big 4 overloaded
 */

#include <iostream>

using std::cout;          

class SomeClass{
   public:

      // 1. Default constructor
      SomeClass(){ 
         cout << "Default constructor called...\n";
      }


      // 2. Destructor
      ~SomeClass(){
         cout << "Destructor called...\n";
      }


      // 3. Copy constructor  
      SomeClass( const SomeClass& ){        
         cout << "Copy constructor called...\n";
      }


      // 4. Assignment operator
      SomeClass& operator=( const SomeClass& ){
         cout << "Assignment operator called...\n";
      }

};



int main(){

   SomeClass a;
   SomeClass clone(a);

   // Note: At this point, copyOfClone needs to be created...
   SomeClass copyOfClone = clone;
   // ... whereas here copyOfClone already exists and therefore
   // DOESN NOT need to be created.
   copyOfClone = clone;

   return 0;
}
